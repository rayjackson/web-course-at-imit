<%-- 
    Document   : response
    Created on : 24.11.2018, 11:55:26
    Author     : rayjackson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <jsp:useBean id="mybean" scope="session" class="org.mypackage.hello.NameHandler" />
            <jsp:setProperty name="mybean" property="name" />
            <div class="jumbotron">
                <h1>Hello, <jsp:getProperty name="mybean" property="name" />!</h1>
            </div>
        </div>        
    </body>
</html>
