<%-- 
    Document   : index
    Created on : 24.11.2018, 11:43:22
    Author     : rayjackson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h1>Entry Form</h1>
            </div>
            <form class="form-group" name="Name Input Form" action="response.jsp">
                <label>Enter your name:</label>
                <input class="form-control mb-3" type="text" name="name" value="" />
                <input class="btn btn-secondary btn-lg d-block px-4 ml-auto mr-auto" type="submit" value="OK" />
            </form>
        </div>
    </body>
</html>
