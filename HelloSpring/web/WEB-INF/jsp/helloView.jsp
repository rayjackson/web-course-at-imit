<%-- 
    Document   : helloView
    Created on : 19.11.2018, 23:15:54
    Author     : rayjackson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hello</title>
    </head>
    <body>
        <h1>${helloMessage}</h1>
    </body>
</html>
