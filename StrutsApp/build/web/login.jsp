<%-- 
    Document   : login
    Created on : 18.11.2018, 20:34:27
    Author     : rayjackson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Форма входа в систему</title>
</head>

<body>
    <h1>Форма входа в систему</h1>
    <html:form action="/login">
        <bean:write name="LoginForm" property="error" filter="false"/>                   
        <div class="form-control">
            <label for="name">Your name: </label>
            <html:text styleId="name" property="name" />
        </div>
        <div class="form-control">
            <label for="email">Your email: </label>
            <html:text styleId="email" property="email" />
        </div>
        <html:submit value="Login" />
    </html:form>
</body>

</html>